
#include <QtCore>

#include <pmdp/pmdp.h>

#include <pmdapplication.h>
#include <pmdcert.h>
#include <pmdpacket.h>
#include <pmdpacketmodel.h>

PMDApplication::PMDApplication(int &argc, char **argv)
	: QApplication(argc, argv), crtmutex(QMutex::Recursive), pktmutex(QMutex::Recursive)
{

	QCoreApplication::setOrganizationName("Vagrearg");
	QCoreApplication::setOrganizationDomain("pingmydroid.org");
	QCoreApplication::setApplicationName("QPmd");
	settings = new QSettings();
	settings->setDefaultFormat(QSettings::IniFormat);

	settings->beginGroup("certificates");
	QStringList grps = settings->childGroups();
	for(int i = 0; i < grps.size(); i++) {
		if(settings->contains(grps[i] + "/crt")) {
			QVariant crtdata = settings->value(grps[i] + "/crt");
			pmdp_x509_crt_t *crt = pmdp_x509_crt_load_pem(crtdata.toByteArray().constData(), crtdata.toByteArray().size());
			const PMDCert *c;
			if(!crt || !(c = addCrt(crt)))
				continue;
printf("Loaded %s\n", c->getFingerprint().toUtf8().constData());
			if(settings->contains(grps[i] + "/key")) {
				QVariant key = settings->value(grps[i] + "/key");
				((PMDCert *)c)->setKeyData(key.toByteArray().constData(), key.toByteArray().size());
			}
		}
	}
	settings->endGroup();
	model = new PMDPacketModel();
}

PMDApplication::~PMDApplication()
{
	QList<PMDCert *> vals = certs.values();
	for(int i = 0; i < vals.size(); i++)
		delete vals[i];
	delete model;
}

const PMDCert *PMDApplication::findCert(const QString &fp)
{
	QMutexLocker lck(&crtmutex);
	return certs.contains(fp) ? certs[fp] : NULL;
}

const PMDCert *PMDApplication::findCert(const void *fpdata, size_t fpdatalen)
{
	QString s;
	for(size_t i = 0; i < fpdatalen; i++)
		s += QString::number((uint)(((const unsigned char *)fpdata)[i]), 16);
	return findCert(s);
}

const PMDCert *PMDApplication::addCrt(PMDCert *cp)
{
	QMutexLocker lck(&crtmutex);

	if(!cp->isValid()) {
		delete cp;
		return NULL;
	}
	const PMDCert *c;
	if(NULL != (c = findCert(cp->getFingerprint()))) {
		delete cp;
		return c;
	}
	certs.insert(cp->getFingerprint(), cp);
	saveCert(cp);
	return cp;
}

const PMDCert *PMDApplication::addCrt(pmdp_x509_crt_t *crt)
{
	QMutexLocker lck(&crtmutex);
	const PMDCert *c;
	if(!crt)
		return NULL;
	PMDCert *cp = new PMDCert(crt);
	if(NULL != (c = findCert(cp->getFingerprint()))) {
		pmdp_x509_crt_free(crt);
		delete cp;
		return c;
	}
	c = cp;
	certs.insert(cp->getFingerprint(), cp);
	saveCert(cp);
	return c;
}

void PMDApplication::saveCert(PMDCert *cp)
{
	if(!settings->contains(QString("certificates/%1/crt").arg(cp->getFingerprint()))) {
		char *buf;
		size_t bufsize;
		if(!pmdp_x509_crt_write(cp->getCrt(), (void **)&buf, &bufsize)) {
			settings->setValue(QString("certificates/%1/crt").arg(cp->getFingerprint()), QByteArray(buf, bufsize));
			free(buf);
			if(cp->getKey()) {
				settings->setValue(QString("certificates/%1/key").arg(cp->getFingerprint()), QByteArray(cp->keyData(), cp->keyDataSize()));
			}
		}
	}
}

int PMDApplication::addPacket(PMDPacket *pkt)
{
	QMutexLocker lck(&pktmutex);
	if(!pkt->isValid())
		return 0;

	if(packets.contains(pkt->getUuid().toString()))
		return 0;

	packets.insert(pkt->getUuid().toString(), pkt);
	packetlist.append(pkt);
	model->insertPacket(pkt);
	return 1;
}

PMDPacket *PMDApplication::getPacket(int idx)
{
	QMutexLocker lck(&pktmutex);
	if(!packetlist.size())
		return NULL;
	if(idx < 0 || idx > packetlist.size())
		return packetlist.last();
	return packetlist.at(idx);
}
