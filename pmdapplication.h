
#ifndef __QPMD_PMDAPPLICATION_H
#define __QPMD_PMDAPPLICATION_H

#include <QApplication>

#define pmdApp	((PMDApplication *)qApp)

class PMDCert;
class PMDPacket;
class PMDPacketModel;

class PMDApplication : public QApplication
{
public:
	PMDApplication(int &argc, char **argv);
	~PMDApplication();

	const PMDCert *findCert(const QString &fp);
	const PMDCert *findCert(const void *fpdata, size_t fpdatalen);

	const PMDCert *addCrt(pmdp_x509_crt_t *crt);
	const PMDCert *addCrt(PMDCert *crt);

	int addPacket(PMDPacket *pkt);
	PMDPacket *getPacket(int idx);

	PMDPacketModel *getModel()	{ return model; }

private:
	QMutex	crtmutex;
	QMutex	pktmutex;

	QMap<QString, PMDCert *>	certs;		// Certificates we know
	QMap<QString, PMDPacket *>	packets;	// Packet UUIDs we received (for filtering dups)
	QList<PMDPacket *>		packetlist;	// Packets we received

	PMDPacketModel			*model;		// The packet model for viewing packet content

	QSettings	*settings;			// Global configuration settings

	void saveCert(PMDCert *cp);
};

#endif
