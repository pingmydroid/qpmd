#include <QtCore>

#include <pmdp/pmdp.h>

#include "pmdcert.h"

static int nullpasswd(char *s, int slen, void *arg)
{
	(void)slen;
	*s = 0;
	((PMDCert *)arg)->needPw();
	return 0;
}

static int knownpasswd(char *s, int slen, void *arg)
{
	QByteArray ba = ((PMDCert *)arg)->pwref->toUtf8();
	int bas = ba.size();
	memcpy(s, ba.constData(), bas > slen ? slen : bas);
	ba.fill('*');
	return bas;
}

PMDCert::PMDCert(const QString &crtname, const QString &keyname)
{
	needpw = false;
	crtfile = crtname;
	keyfile = keyname;
	key = NULL;
	crt = NULL;
	keydata = NULL;

	loadCrt();
	calcFingerprint();

	// Load the key data
	QFile k(keyname);
	if(!k.open(QIODevice::ReadOnly))
		return;
	keydatalen = k.size();
	keydata = new char[keydatalen];
	QDataStream s(&k);
	if(-1 == s.readRawData(keydata, keydatalen)) {
		fprintf(stderr, "%s: Unexpected error reading PEM data\n", keyname.toUtf8().constData());
		delete[] keydata;
		keydata = NULL;
		needpw = false;
		return;
	}

	loadKey(QString());	// Try load the key without password
}

PMDCert::PMDCert(pmdp_x509_crt_t *c)
{
	needpw = false;
	key = NULL;
	keydata = NULL;
	crt = c;
	calcFingerprint();
}

void PMDCert::calcFingerprint()
{
	fingerprint = QString();
	if(!crt)
		return;
	uint8_t *buf;
	size_t sz;
	if(pmdp_x509_fingerprint(crt, &buf, &sz))
		return;
	fingerprint = "";
	for(size_t i = 0; i < sz; i++)
		fingerprint += QString::number((uint)buf[i], 16);
}

PMDCert::~PMDCert()
{
	if(keydata)
		delete[] keydata;
	if(key)
		pmdp_x509_key_free(key);
	if(crt)
		pmdp_x509_crt_free(crt);
}

const pmdp_x509_crt_t *PMDCert::loadCrt()
{
	crt = pmdp_x509_crt_load_file(crtfile.toUtf8().constData());
	return crt;
}

const pmdp_x509_key_t *PMDCert::loadKey(const QString &pw)
{
	if(!key && keydata) {
		pwref = &pw;
		return key = pmdp_x509_key_load_pem(keydata, keydatalen, knownpasswd, this);
	} else if(key)
		return key;
	else
		return key = NULL;
}

void PMDCert::setKeyData(const char *data, qint64 datalen)
{
	Q_ASSERT(key == NULL);
	Q_ASSERT(keydata == NULL);
	keydata = new char[datalen];
	keydatalen = datalen;
	memcpy(keydata, data, datalen);
}

