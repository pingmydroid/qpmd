
#ifndef __QPMDP_PMDCERT_H
#define __QPMDP_PMDCERT_H

class PMDCert
{
public:
	PMDCert(const QString &crtname, const QString &keyname);
	PMDCert(pmdp_x509_crt_t *c);
	virtual ~PMDCert();

	const pmdp_x509_key_t *getKey()	const { return key; }
	const pmdp_x509_crt_t *getCrt()	const { return crt; }

	const pmdp_x509_crt_t *loadCrt();
	const pmdp_x509_key_t *loadKey(const QString &pw);

	void setKeyData(const char *data, qint64 datalen);

	void needPw()			{ needpw = true; };
	bool isValid()			{ return crt != NULL; }
	QString getFingerprint() const	{ return fingerprint; }

	char *keyData()			{ return keydata; }
	qint64 keyDataSize()		{ return keydatalen; }

private:
	pmdp_x509_key_t	*key;
	pmdp_x509_crt_t	*crt;
	bool		needpw;
	QString		crtfile;
	QString		keyfile;
	char		*keydata;
	qint64		keydatalen;
	QString		fingerprint;

	void calcFingerprint();

public:
	const QString	*pwref;
};

#endif
