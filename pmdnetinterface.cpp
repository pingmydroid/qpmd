
#include <pmdnetinterface.h>

PMDNetInterface::PMDNetInterface(const QNetworkInterface &iface)
{
	netiface = iface;
	flags = 0;

	Q_ASSERT(iface.flags() & QNetworkInterface::IsUp);

	if(iface.flags() & QNetworkInterface::CanBroadcast)
		flags |= PMDNI_BC;
	if(iface.flags() & QNetworkInterface::CanMulticast)
		flags |= PMDNI_MC;

	QListIterator<QNetworkAddressEntry> la(iface.addressEntries());
	while(la.hasNext()) {
		handleAddress(la.next());
	}

}

PMDNetInterface::~PMDNetInterface()
{
}

#define IS_IN_MC(x)	(((x) & 0xf0000000) == 0xe0000000)
#define IS_IN6_MC(x)	((x)[0] == 0xff)

void PMDNetInterface::handleAddress(const QNetworkAddressEntry &addr)
{
	QHostAddress ha = addr.ip();
//printf("Address: %s/%d\n", ha.toString().toUtf8().constData(), addr.prefixLength());
	if(ha.protocol() == QAbstractSocket::IPv4Protocol) {
		flags |= PMDNI_INET;
		if(ha.isNull() || IS_IN_MC(ha.toIPv4Address()))
			return;
		QListIterator<QHostAddress> li(ipv4addrs);
		quint32 mask = addr.netmask().toIPv4Address();
		while(li.hasNext()) {
			QHostAddress a = li.next();
			quint32 ip = a.toIPv4Address();
			if((ha.toIPv4Address() & mask) == (ip & mask))
				return;
		}
		ipv4addrs.append(ha);
	} else if(ha.protocol() == QAbstractSocket::IPv6Protocol) {
		flags |= PMDNI_INET6;
		if(ha.isNull() || IS_IN6_MC(ha.toIPv6Address()))
			return;
		QListIterator<QHostAddress> li(ipv6addrs);
		Q_IPV6ADDR mask = addr.netmask().toIPv6Address();
		while(li.hasNext()) {
			QHostAddress a = li.next();
			Q_IPV6ADDR ipx = ha.toIPv6Address();
			Q_IPV6ADDR ipy = a.toIPv6Address();
			int i;
			for(i = 0; i < 16; i++) {
				if((ipx[i] & mask[i]) != (ipy[i] & mask[i]))
					break;
			}
			if(i == 16)
				return;
		}
		ipv6addrs.append(ha);
	}
}

PMDNetInterface *PMDNetInterface::handleInterface(const QNetworkInterface &iface)
{
	/* Ignore loopback and p2p interfaces */
	if(iface.flags() & (QNetworkInterface::IsLoopBack | QNetworkInterface::IsPointToPoint))
		return NULL;

	/* Ignore if the interface is not up */
	if(!(iface.flags() & QNetworkInterface::IsUp))
		return NULL;

	/* Ignore if the interface can not do multicast nor broadcast */
	if(!(iface.flags() & (QNetworkInterface::CanMulticast | QNetworkInterface::CanBroadcast)))
		return NULL;

	PMDNetInterface *nif = new PMDNetInterface(iface);
	if(!nif->hasIP() || !nif->hasXcast()) {
		/* No (usable) IP networking on the interface, ignore */
		delete nif;
		return NULL;
	}

	return nif;
}

/*
 * Return a list of "up" interfaces with IPv[46] addresses
 */
QList<PMDNetInterface *> PMDNetInterface::enumInterfaces()
{
	QList<QNetworkInterface> iflist = QNetworkInterface::allInterfaces();
	QListIterator<QNetworkInterface> li(iflist);
	QList<PMDNetInterface *> ifs;
	while(li.hasNext()) {
		PMDNetInterface *nif = handleInterface(li.next());
		if(nif)
			ifs.append(nif);
	}
	return ifs;
}
