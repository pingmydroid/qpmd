
#ifndef __QPMDP_PMDINTERFACE_H
#define __QPMDP_PMDINTERFACE_H

#include <QList>
#include <QNetworkInterface>

class PMDNetInterface
{
public:
	PMDNetInterface(const QNetworkInterface &iface);
	virtual ~PMDNetInterface();

	/* Flags */
	enum {
		PMDNI_INET = 0x01,
		PMDNI_INET6 = 0x02,
		PMDNI_MC = 0x04,
		PMDNI_BC = 0x08,
	};
private:
	QNetworkInterface	netiface;
	unsigned		flags;
public:
	QList<QHostAddress>	ipv4addrs;
	QList<QHostAddress>	ipv6addrs;

public:
	bool hasIP()		{ return 0 != (flags & (PMDNI_INET | PMDNI_INET6)); };
	bool hasXcast()		{ return 0 != (flags & (PMDNI_MC | PMDNI_BC)); };
	unsigned getFlags()	{ return flags; };
	QString getName()	{ return netiface.name(); };
	const QNetworkInterface &interface()	{ return netiface; };
private:
	void handleAddress(const QNetworkAddressEntry &addr);

public:
	static QList<PMDNetInterface *> enumInterfaces();

private:
	static PMDNetInterface *handleInterface(const QNetworkInterface &iface);
};

#endif
