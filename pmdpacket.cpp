#include <QtCore>
#include <QHostInfo>
#include <QHostAddress>

#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pmdp/pmdp.h>

#include <pmdpacket.h>
#include <pmdapplication.h>
#include <pmdcert.h>

static int certcb(pmdp_x509_crt_t *crt, void *ptr)
{
	(void)ptr;
	return 0 == pmdApp->addCrt(crt);
}

static int mkparts(const pmdp_msg_t *msg, const pmdp_msg_part_t *part, void *arg)
{
	(void)msg;
	PMDPacket *p = (PMDPacket *)arg;
	switch(part->type) {
	case PMDT_CATEGORY:	p->addPart(new PMDPacketPartCategory(part, p)); break;
	case PMDT_SUBCATEGORY:	p->addPart(new PMDPacketPartSubCategory(part, p)); break;
	case PMDT_TEXT:		p->addPart(new PMDPacketPartText(part, p)); break;
	case PMDT_RESOURCE:	p->addPart(new PMDPacketPartResource(part, p)); break;
	case PMDT_UUID:		p->addPart(new PMDPacketPartUuid(part, p)); break;
	case PMDT_TIMESTAMP:	p->addPart(new PMDPacketPartTimestamp(part, p)); break;
	case PMDT_CARDINAL:	p->addPart(new PMDPacketPartCardinal(part, p)); break;
	case PMDT_INTEGER:	p->addPart(new PMDPacketPartInteger(part, p)); break;
	case PMDT_FRACTION:	p->addPart(new PMDPacketPartFraction(part, p)); break;
	case PMDT_PADDING:	p->addPart(new PMDPacketPartPadding(part, p)); break;
	case PMDT_KEY:		p->addPart(new PMDPacketPartKey(part, p)); break;
	case PMDT_ENCRYPTED:	p->addPart(new PMDPacketPartEncrypted(part, p)); break;
	case PMDT_FINGERPRINT:	p->addPart(new PMDPacketPartFingerprint(part, p)); break;
	case PMDT_SIGNATURE:	p->addPart(new PMDPacketPartSignature(part, p)); break;
	default:		p->addPart(new PMDPacketPartUnknown(part, p)); break;
	}
	return 1;
}

PMDPacket::PMDPacket(char *data, qint64 datalen, const QHostAddress &from, quint16 fromport, QObject *parent)
	: QObject(parent), nullpart(NULL, NULL, parent), arrival(QDateTime::currentDateTime())
{
	peer = from;
	peerport = fromport;
	pkt = data;
	pktlen = datalen;
	msg = NULL;
	seen = false;

	/* Lookup source of the packet */
	peerinfo = QHostInfo::fromName(peer.toString());

	/* Parse the packet */
	parseflags = PMDP_PARSE_UTYPES | PMDP_PARSE_TRAILING | PMDP_PARSE_BADUTF8;
	err = pmdp_msg_parse(pkt, (size_t)pktlen, &msg, &parseflags);
	if(err < 0) {
		errmsg = QString(pmdp_strerror(err));
		return;
	}

	/* Check the signature */
	if(parseflags & PMDP_PARSE_HAVESIG) {
		const PMDCert *fpc;
		pmdp_msg_part_t *fppart;
		if(!(fppart = pmdp_msg_get_fingerprint(msg))) {
			err = PMDP_ERR_FINGERPRINT;
			errmsg = tr("Cannot retrieve message's fingerprint");
		} else {
			if(!(fpc = pmdApp->findCert(fppart->octets, fppart->length))) {
				struct sockaddr_in sin;
				struct sockaddr_in6 sin6;
				const struct sockaddr *sap;
				socklen_t sapl;
				if(peer.protocol() == QAbstractSocket::IPv4Protocol) {
					sin.sin_port = htons(peerport);
					sin.sin_addr.s_addr = htonl(peer.toIPv4Address());
					sapl = sizeof(sin);
					sap = (const struct sockaddr *)&sin;
				} else {
					sin6.sin6_port = htons(peerport);
					Q_IPV6ADDR a = peer.toIPv6Address();
					memcpy(&sin6.sin6_addr, &a, 16);
					sapl = sizeof(sin6);
					sap = (const struct sockaddr *)&sin6;
				}
				pmdp_dns_crt_by_addr(sap, sapl, certcb, NULL);
				if((fpc = pmdApp->findCert(fppart->octets, fppart->length)))
					goto found_cert;
			} else {
found_cert:
				if(!(validsig = 0 == (err = pmdp_msg_verify(msg, fpc->getCrt())))) {
					errmsg = QString(tr("warning: Message signature error: %1")).arg(pmdp_strerror(err));
				}
			}
		}
	}

	if(isValid()) {
		uuid = QUuid::fromRfc4122(QByteArray((const char *)msg->uuid, sizeof(msg->uuid)));
		pmdp_msg_iterate_parts(msg, mkparts, this);
	}
}

PMDPacket::~PMDPacket()
{
	while(parts.size())
		delete parts.takeLast();
	if(msg)
		pmdp_msg_free(msg);
	delete[] pkt;
}

const QString PMDPacket::getSeverityString() const
{
	switch(getSeverity()) {
	case QPMDS_EMERGENCY:	return QString(tr("Emergency"));
	case QPMDS_ALERT:	return QString(tr("Alert"));
	case QPMDS_CRITICAL:	return QString(tr("Critical"));
	case QPMDS_ERROR:	return QString(tr("Error"));
	case QPMDS_WARNING:	return QString(tr("Warning"));
	case QPMDS_NOTICE:	return QString(tr("Notice"));
	case QPMDS_INFO:	return QString(tr("Info"));
	case QPMDS_DEBUG:	return QString(tr("Debug"));
	default:		return QString(tr("<invalid severity>"));
	}
}

const PMDPacketPart *PMDPacket::getPart(unsigned type) const
{
	for(int i = 0; i < parts.size(); i++) {
		if(parts[i]->getType() == type)
			return parts[i];
	}
	return &nullpart;
}

