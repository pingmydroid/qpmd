

#ifndef __QPMDP_PMDPACKET_H
#define __QPMDP_PMDPACKET_H

#include <QDateTime>
#include <QHostAddress>
#include <QHostInfo>
#include <QUuid>

#include <pmdp/pmdp.h>

#include <pmdpacketpart.h>

enum PMDSeverity {
	QPMDS_EMERGENCY,
	QPMDS_ALERT,
	QPMDS_CRITICAL,
	QPMDS_ERROR,
	QPMDS_WARNING,
	QPMDS_NOTICE,
	QPMDS_INFO,
	QPMDS_DEBUG,
};

class PMDPacketPart;

class PMDPacket : public QObject
{
	Q_OBJECT

public:
	PMDPacket(char *data, qint64 datalen, const QHostAddress &from, quint16 fromport, QObject *parent = 0);
	virtual ~PMDPacket();

	bool isValid() const		{ return err == 0 && msg != NULL; }
	unsigned getVersion() const	{ return isValid() ? msg->version : 0; }
	PMDSeverity getSeverity() const	{ return (PMDSeverity)(isValid() ? msg->severity : 0); }
	const QString getSeverityString() const;
	unsigned getSequence() const	{ return isValid() ? msg->sequence : 0; }
	unsigned getFlags() const	{ return isValid() ? msg->flags : 0; }
	QDateTime getTimestamp() const	{ return QDateTime::fromMSecsSinceEpoch(isValid() ? (msg->timestamp.tv_sec * 1000 + msg->timestamp.tv_usec / 1000) : 0); }
	QUuid getUuid() const		{ return uuid; }
	bool haveEncrypted() const	{ return isValid() && 0 != (parseflags & PMDP_PARSE_HAVEENCR); }
	bool forceSilent() const	{ return isValid() && (msg->flags & PMDF_FORCESILENT) != 0; };

	const PMDPacketPart *getPart(unsigned type) const;
	const PMDPacketPart *getCategory()	{ return getPart(PMDT_CATEGORY); };
	const PMDPacketPart *getSubCategory()	{ return getPart(PMDT_SUBCATEGORY); };
	const QList<PMDPacketPart *> *getParts() const	{ return &parts; }
	int getPartCount() const			{ return parts.size(); }

	void addPart(PMDPacketPart *p)	{ parts.append(p); }

	bool haveSeen()			{ return seen; }
	void setSeen(bool b = true)	{ seen = b; }
	bool validSignature()		{ return validsig; }

	const QDateTime getArrival()	{ return arrival; }

private:
	QHostAddress		peer;		// Sender's address
	quint16			peerport;
	QHostInfo		peerinfo;
	char			*pkt;		// Binary packet data
	qint64			pktlen;
	pmdp_msg_t		*msg;		// PMD library interpreted packet
	int			err;		// PMD library error (0 if ok)
	QString			errmsg;		// PMD library error message
	bool			validsig;	// Set if singature verified
	unsigned		parseflags;	// PMD library parser flags
	QUuid			uuid;		// Message's UUID
	QList<PMDPacketPart *>	parts;		// Deconstructed message content parts
	PMDPacketPartNull	nullpart;	// Helper for unknowns
	bool			seen;		// Set if packet inspected by user
	QDateTime		arrival;	// TImestamp when the packed was received
};

#endif
