#include <QtGui>

#include <pmdpacketdelegate.h>
#include <pmdpacketitem.h>

PMDPacketDelegate::PMDPacketDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{
}

QWidget *PMDPacketDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem & /*option*/, const QModelIndex &index) const
{
	if(!index.isValid())
		return NULL;
	QLabel *label = new QLabel(parent);
	PMDPacketItem *pi = (PMDPacketItem *)index.internalPointer();
	if(pi->getPart() && pi->getPart()->getType() == PMDT_RESOURCE) {
		label->setTextFormat(Qt::RichText);
		label->setTextInteractionFlags(Qt::TextBrowserInteraction);
		label->setOpenExternalLinks(true);
	} else
		label->setTextInteractionFlags(Qt::TextSelectableByMouse);
	label->setAutoFillBackground(true);
	label->show();
	return label;
}

void PMDPacketDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	QLabel *label = qobject_cast<QLabel *>(editor);
	if(!label)
		return;
	label->setText(index.model()->data(index).toString());
}

