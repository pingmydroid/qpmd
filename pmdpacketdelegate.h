
#ifndef __QPMD_PMDPACKETDELEGATE_H
#define __QPMD_PMDPAKCETDELEGATE_H

#include <QStyledItemDelegate>

class PMDPacketDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	PMDPacketDelegate(QObject *parent = 0);

	QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
	void setEditorData(QWidget *editor, const QModelIndex &index) const;
};

#endif
