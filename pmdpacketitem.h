
#ifndef __QPMD_PMDPACKETITEM_H
#define __QPMD_PMDPAKCETITEM_H

#include <pmdpacket.h>
#include <pmdpacketpart.h>

class PMDPacketItem
{
public:
	PMDPacketItem(const PMDPacket *p) {
		pkt = p;
		part = NULL;
		parent = NULL;
		const QList<PMDPacketPart *> *ppl = p->getParts();
		for(int i = 0; i < ppl->size(); i++)
			children.append(new PMDPacketItem((*ppl)[i], this));
	};
	virtual ~PMDPacketItem() {
		qDeleteAll(children);
	};

private:
	PMDPacketItem(const PMDPacketPart *pt, PMDPacketItem *pa) {
		pkt = NULL;
		part = pt;
		parent = pa;
	};
public:
	int childCount()		{ return children.size(); }
	PMDPacketItem *getParent()	{ return parent; }
	PMDPacketItem *childAt(int i)	{ return children[i]; }
	const PMDPacket *getPacket()	{ return pkt; }
	const PMDPacketPart *getPart()	{ return part; }

private:
	const PMDPacket		*pkt;
	const PMDPacketPart	*part;
	PMDPacketItem		*parent;
	QList<PMDPacketItem *>	children;
};

#endif
