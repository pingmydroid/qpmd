#include <QtGui>
#include <pmdpacketmodel.h>
#include <pmdpacketitem.h>

PMDPacketModel::PMDPacketModel(QObject *parent)
	: QAbstractItemModel(parent)
{
}

PMDPacketModel::~PMDPacketModel()
{
	qDeleteAll(items);
}

QModelIndex PMDPacketModel::index(int row, int column, const QModelIndex &parent) const
{
	if(column < 0 || column > 1 || row < 0)
		return QModelIndex();

	if(!parent.isValid()) {
		if(row >= items.size())
			return QModelIndex();
		return createIndex(row, column, items[row]);
	} else {
		PMDPacketItem *pi = (PMDPacketItem *)parent.internalPointer();
		if(!pi)
			return QModelIndex();
		if(row >= pi->childCount())
			return QModelIndex();
		return createIndex(row, column, pi->childAt(row));
	}
}

QModelIndex PMDPacketModel::parent(const QModelIndex &index) const
{
	if(!index.isValid())
		return QModelIndex();

	PMDPacketItem *pi = (PMDPacketItem *)index.internalPointer();
	if(!pi)
		return QModelIndex();
	PMDPacketItem *pip = pi->getParent();
	if(pip) {
		int i = items.indexOf(pip);
		return i != -1 ? createIndex(i, 0, pip) : QModelIndex();
	} else
		return QModelIndex();
}

int PMDPacketModel::rowCount(const QModelIndex &parent) const
{
	if(!parent.isValid())
		return items.size();

	PMDPacketItem *pi = (PMDPacketItem *)parent.internalPointer();
	return pi->childCount();
}

int PMDPacketModel::columnCount(const QModelIndex &parent) const
{
	(void)parent;
	return 2;
}

QVariant PMDPacketModel::data(const QModelIndex &index, int role) const
{
	PMDPacketItem *pi;
	if(!index.isValid())
		return QVariant();

	pi = (PMDPacketItem *)index.internalPointer();
	PMDPacketItem *parent = pi->getParent();

	int col = index.column();
	if(col < 0 || col > 1)
		return QVariant();

	if(!parent) {
		switch(role) {
		case Qt::DisplayRole:
			return QVariant(!col ? pi->getPacket()->getTimestamp().toString() : pi->getPacket()->getSeverityString());
		case Qt::DecorationRole:
			if(col)
				return QVariant();
			switch(pi->getPacket()->getSeverity()) {
			case PMDS_EMERGENCY:
			case PMDS_CRITICAL:
			case PMDS_ERROR:
				return QVariant(QIcon::fromTheme("dialog-error"));
			case PMDS_WARNING:
				return QVariant(QIcon::fromTheme("dialog-warning"));
			default:
				return QVariant(QIcon::fromTheme("dialog-information"));
			}
			break;
		}
	} else {
		switch(role) {
		case Qt::DisplayRole:
			if(col && pi->getPart()->getType() == PMDT_RESOURCE) {
				return QVariant(QString("<a href=\"%1\">%2</a>").arg(pi->getPart()->toString().replace('"', "%22")).arg(pi->getPart()->toString()));
			} else
				return QVariant(!col ? pi->getPart()->getTypeName() : pi->getPart()->toString());
			break;
		}
	}
	return QVariant();
}

Qt::ItemFlags PMDPacketModel::flags(const QModelIndex &index) const
{
	if(!index.isValid())
		return 0;
	return Qt::ItemIsEnabled /*| Qt::ItemIsEditable*/;
}

void PMDPacketModel::insertPacket(const PMDPacket *pkt)
{
	beginInsertRows(QModelIndex(), 0, 0);
	items.prepend(new PMDPacketItem(pkt));
	endInsertRows();
}

