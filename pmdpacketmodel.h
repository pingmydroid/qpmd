
#ifndef __QPMD_PMDPACKETMODEL_H
#define __QPMD_PMDPAKCETMODEL_H

#include <QAbstractItemModel>

class PMDPacketItem;
class PMDPacket;

class PMDPacketModel : public QAbstractItemModel
{
	Q_OBJECT
public:
	PMDPacketModel(QObject *parent = 0);
	~PMDPacketModel();

	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;

	void insertPacket(const PMDPacket *pkt);

private:
	QList<PMDPacketItem *> items;
};

#endif
