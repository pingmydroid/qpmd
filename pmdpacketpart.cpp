
#include <QtCore>

#include <pmdp/pmdp.h>

#include <pmdpacket.h>
#include <pmdpacketpart.h>

PMDPacketPart::PMDPacketPart(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: QObject(parent)
{
	owner = msg;
	part = msgpart;
}

PMDPacketPart::~PMDPacketPart()
{
}

QString PMDPacketPart::getTypeName() const
{
	switch(getType()) {
	case PMDT_CATEGORY:	return QString(tr("Category"));
	case PMDT_SUBCATEGORY:	return QString(tr("Sub-category"));
	case PMDT_TEXT:		return QString(tr("Text"));
	case PMDT_RESOURCE:	return QString(tr("Resource"));
	case PMDT_UUID:		return QString(tr("UUID"));
	case PMDT_TIMESTAMP:	return QString(tr("Timestamp"));
	case PMDT_CARDINAL:	return QString(tr("Cardinal"));
	case PMDT_INTEGER:	return QString(tr("Integer"));
	case PMDT_FRACTION:	return QString(tr("Fraction"));
	case PMDT_PADDING:	return QString(tr("Padding"));
	case PMDT_KEY:		return QString(tr("Key"));
	case PMDT_ENCRYPTED:	return QString(tr("Encrypted"));
	case PMDT_FINGERPRINT:	return QString(tr("Fingerprint"));
	case PMDT_SIGNATURE:	return QString(tr("Signature"));
	default:		return QString(tr("<unknown type 0x%1>")).arg(getType(), 8, 16);
	}
}

PMDPacketPartCategory::PMDPacketPartCategory(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartCategory::toString(unsigned cat) const
{
	(void)cat;
	switch(part->category) {
	case PMDC_LOCAL:	return QString(tr("Local"));
	case PMDC_MONITOR:	return QString(tr("Monitor"));
	case PMDC_LOG:		return QString(tr("Log"));
	case PMDC_TRAFFIC:	return QString(tr("Traffic"));
	case PMDC_PUBLIC:	return QString(tr("Public"));
	case PMDC_COMMERCIAL:	return QString(tr("Commercial"));
	case PMDC_SERVICE:	return QString(tr("Service"));
	default:		return QString(tr("<unknown category 0x%1>")).arg(part->category, 8, 16);
	}
}

PMDPacketPartSubCategory::PMDPacketPartSubCategory(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartSubCategory::toString(unsigned cat) const
{
	if(part->subcategory & PMDC_SUBCAT_PRIVATE)
		return QString(tr("(private subcategory 0x%1)")).arg(part->subcategory, 8, 16);

	switch(cat) {
	case PMDC_LOCAL:
		switch(part->subcategory) {
		case PMDC_LOCAL_NONE:	return QString(tr("None"));
		case PMDC_LOCAL_BELL:	return QString(tr("Bell"));
		case PMDC_LOCAL_DINNER:	return QString(tr("Dinner"));
		default:		return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
		}
		break;
	case PMDC_MONITOR:
		switch(part->subcategory) {
		case PMDC_MONITOR_NONE:		return QString(tr("None"));
		case PMDC_MONITOR_TEMPERATURE:	return QString(tr("Dinner"));
		default:			return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
		}
		break;
	case PMDC_LOG:		return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
	case PMDC_TRAFFIC:	return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
	case PMDC_PUBLIC:	return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
	case PMDC_COMMERCIAL:	return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
	case PMDC_SERVICE:
		switch(part->subcategory) {
		case PMDC_SERVICE_NONE:		return QString(tr("None"));
		case PMDC_SERVICE_READYTOORDER:	return QString(tr("Ready to order"));
		case PMDC_SERVICE_CHECKPLEASE:	return QString(tr("Check please"));
		case PMDC_SERVICE_QUEUEREQUEST:	return QString(tr("Queue request"));
		case PMDC_SERVICE_QUEUEASSIGN:	return QString(tr("Queue assign"));
		case PMDC_SERVICE_QUEUESTATUS:	return QString(tr("Queue status"));
		default:			return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
		}
		break;
	default:		return QString(tr("<unknown subcategory 0x%1>")).arg(part->subcategory, 8, 16);
	}

}

PMDPacketPartInteger::PMDPacketPartInteger(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartInteger::toString(unsigned cat) const
{
	(void)cat;
	return QString("%1").arg(part->integer);
}

PMDPacketPartCardinal::PMDPacketPartCardinal(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartCardinal::toString(unsigned cat) const
{
	(void)cat;
	return QString("%1").arg(part->cardinal);
}

PMDPacketPartUuid::PMDPacketPartUuid(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
	uuid = QUuid::fromRfc4122(QByteArray((const char *)msgpart->uuid, sizeof(msgpart->uuid)));
}

QString PMDPacketPartUuid::toString(unsigned cat) const
{
	(void)cat;
	return uuid.toString();
}

PMDPacketPartText::PMDPacketPartText(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartText::toString(unsigned cat) const
{
	(void)cat;
	return QString(QByteArray(part->string, part->length));
}

PMDPacketPartResource::PMDPacketPartResource(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartResource::toString(unsigned cat) const
{
	(void)cat;
	return QString(QByteArray(part->string, part->length));
}

PMDPacketPartTimestamp::PMDPacketPartTimestamp(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
	timestamp = QDateTime::fromMSecsSinceEpoch(part->timestamp.tv_sec * 1000 + part->timestamp.tv_usec / 1000);
}

QString PMDPacketPartTimestamp::toString(unsigned cat) const
{
	(void)cat;
	return timestamp.toString("ddd MMMM d yyyy hh:mm:ss.zzz");
}

PMDPacketPartFraction::PMDPacketPartFraction(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartFraction::toString(unsigned cat) const
{
	(void)cat;
	if(!part->fraction.divisor) {
		if(part->fraction.dividend > 0)
			return QString("+Infinite (%1/%2)").arg(part->fraction.dividend).arg(part->fraction.divisor);
		else if(part->fraction.dividend < 0)
			return QString("-Infinite (%1/%2)").arg(part->fraction.dividend).arg(part->fraction.divisor);
		else
			return QString("NaN (%1/%2)").arg(part->fraction.dividend).arg(part->fraction.divisor);
	}
	return QString("%1 (%2/%3)").arg((double)part->fraction.dividend / (double)part->fraction.divisor).arg(part->fraction.dividend).arg(part->fraction.divisor);
}

PMDPacketPartKey::PMDPacketPartKey(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartKey::toString(unsigned cat) const
{
	(void)cat;
	return QString("<encryption key>");
}

PMDPacketPartPadding::PMDPacketPartPadding(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartPadding::toString(unsigned cat) const
{
	(void)cat;
	return QString("<encryption padding>");
}

PMDPacketPartEncrypted::PMDPacketPartEncrypted(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartEncrypted::toString(unsigned cat) const
{
	(void)cat;
	return QString("<encrypted data>");
}

PMDPacketPartFingerprint::PMDPacketPartFingerprint(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartFingerprint::toString(unsigned cat) const
{
	(void)cat;
	QString s;
	for(unsigned i = 0; i < part->length; i++) {
		if(i)
			s += ":";
		s += QString("%1").arg(part->octets[i], 2, 16);
	}
	return s;
}

PMDPacketPartSignature::PMDPacketPartSignature(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartSignature::toString(unsigned cat) const
{
	(void)cat;
	return QString("<signature>");
}

PMDPacketPartUnknown::PMDPacketPartUnknown(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartUnknown::toString(unsigned cat) const
{
	(void)cat;
	return QString("<unknown type 0x%1>").arg(part->type, 4, 16);
}

PMDPacketPartNull::PMDPacketPartNull(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent)
	: PMDPacketPart(msgpart, msg, parent)
{
}

QString PMDPacketPartNull::toString(unsigned cat) const
{
	(void)cat;
	return QString("<unknown>");
}

