
#ifndef __QPMD_PMDPACKETPART_H
#define __QPMD_PMDPACKETPART_H

#include <QObject>
#include <QDateTime>
#include <QUuid>

#include <pmdp/pmdp.h>

class PMDPacket;

class PMDPacketPart : public QObject
{
	Q_OBJECT

public:
	PMDPacketPart(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	~PMDPacketPart();

	virtual QString toString(unsigned cat = 0) const = 0;

	unsigned getType() const	{ return part ? part->type : 0; };
	QString getTypeName() const;

protected:
	const PMDPacket		*owner;
	const pmdp_msg_part_t	*part;
};


class PMDPacketPartCategory : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartCategory(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartSubCategory : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartSubCategory(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartInteger : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartInteger(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartCardinal : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartCardinal(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartUuid : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartUuid(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;

private:
	QUuid	uuid;
};

class PMDPacketPartTimestamp : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartTimestamp(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
private:
	QDateTime	timestamp;
};

class PMDPacketPartFraction : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartFraction(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartKey : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartKey(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartEncrypted : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartEncrypted(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartText : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartText(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartResource : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartResource(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartPadding : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartPadding(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartFingerprint : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartFingerprint(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartSignature : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartSignature(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartUnknown : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartUnknown(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

class PMDPacketPartNull : public PMDPacketPart
{
	Q_OBJECT
public:
	PMDPacketPartNull(const pmdp_msg_part_t *msgpart, const PMDPacket *msg, QObject *parent = 0);
	QString toString(unsigned cat = 0) const;
};

#endif
