
#include <QtCore>

#include <pmdp/pmdp.h>

#include <pmdpacket.h>
#include <pmdpacketrunner.h>

PMDPacketRunner::PMDPacketRunner(char *_data, qint64 _datalen, QHostAddress _ha, quint16 _port, QObject *parent)
	: QObject(parent), QRunnable()
{
	data = _data;
	datalen = _datalen;
	ha = _ha;
	port = _port;
	setAutoDelete(true);
}

void PMDPacketRunner::run()
{
	emit packetReady(new PMDPacket(data, datalen, ha, port));
}

