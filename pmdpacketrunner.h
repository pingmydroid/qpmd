
#ifndef __QPMD_PMDPACKETRUNNER_H
#define __QPMD_PMDPACKETRUNNER_H

#include <QObject>
#include <QRunnable>
#include <QHostAddress>

class PMDPacket;

class PMDPacketRunner : public QObject, public QRunnable
{
	Q_OBJECT

public:
	PMDPacketRunner(char *_data, qint64 _datalen, QHostAddress _ha, quint16 _port, QObject *parent);
	void run();

private:
	char		*data;
	qint64		datalen;
	QHostAddress	ha;
	quint16		port;

signals:
	void packetReady(PMDPacket *);
};

#endif
