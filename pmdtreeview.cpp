#include <QtGui>

#include <pmdtreeview.h>

PMDTreeView::PMDTreeView(QWidget *parent)
	: QTreeView(parent)
{
}

void PMDTreeView::modelReset()
{
printf("model reset\n");
	const int rows = model()->rowCount();
	for(int i = 0; i < rows; ++i) {
		QModelIndex base = model()->index(i, 0);
		const int parts = model()->rowCount(base);
		for(int j = 0; j < parts; j++) {
			QModelIndex idx = model()->index(j, 1, base);
			openPersistentEditor(idx);
		}
	}
}

void PMDTreeView::rowsInserted(const QModelIndex &, int start, int end)
{
printf("model insert %d %d\n", start, end);
	for(int i = start; i <= end; i++) {
		QModelIndex base = model()->index(i, 0);
		const int parts = model()->rowCount(base);
		for(int j = 0; j < parts; j++) {
			QModelIndex idx = model()->index(j, 1, base);
			openPersistentEditor(idx);
		}
	}
	collapseAll();
	resizeColumnToContents(0);
	expand(model()->index(0, 0, QModelIndex()));
}
