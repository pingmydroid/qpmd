

#ifndef __QPMD_PMDTREEVIEW_H
#define __QPMD_PMDTREEVIEW_H

#include <QTreeView>

class PMDTreeView : public QTreeView
{
	Q_OBJECT;

public:
	PMDTreeView(QWidget *parent = 0);

public slots:
	void modelReset();
	void rowsInserted(const QModelIndex &parent, int start, int end);
};

#endif
