CONFIG += link_pkgconfig
PKGCONFIG += libpmdp

HEADERS       = window.h \
		viewwindow.h \
		pmdnetinterface.h \
		pmdpacket.h \
		pmdpacketdelegate.h \
		pmdpacketmodel.h \
		pmdpacketitem.h \
		pmdpacketpart.h \
		pmdpacketrunner.h \
		pmdcert.h \
		pmdtreeview.h \
		pmdapplication.h

SOURCES       = main.cpp \
                window.cpp \
		viewwindow.cpp \
		pmdnetinterface.cpp \
		pmdpacket.cpp \
		pmdpacketdelegate.cpp \
		pmdpacketmodel.cpp \
		pmdpacketpart.cpp \
		pmdpacketrunner.cpp \
		pmdcert.cpp \
		pmdtreeview.cpp \
		pmdapplication.cpp

RESOURCES     = qpmd.qrc
QT           += xml svg network phonon

