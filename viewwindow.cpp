
#include <QtGui>

#include <pmdp/pmdp.h>

#include <pmdapplication.h>
#include <pmdtreeview.h>
#include <pmdpacketmodel.h>
#include <pmdpacketdelegate.h>
#include <viewwindow.h>

ViewWindow::ViewWindow(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_DeleteOnClose);

	tree = new PMDTreeView();
	tree->setHeaderHidden(true);
	tree->setItemDelegate(new PMDPacketDelegate());
	tree->setModel(pmdApp->getModel());
	tree->modelReset();
	connect(tree->model(), SIGNAL(modelReset()), tree, SLOT(modelReset()));
	connect(tree->model(), SIGNAL(rowsInserted(const QModelIndex &, int, int)), tree, SLOT(rowsInserted(const QModelIndex &, int, int)));
	tree->collapseAll();
	tree->resizeColumnToContents(0);
	tree->expand(tree->model()->index(0, 0, QModelIndex()));

	vbox = new QVBoxLayout();
	vbox->addWidget(tree);

	setWindowTitle(tr("PMD Message"));
	setLayout(vbox);
	setWindowIcon(QIcon(":/images/pingmydroid_144x144.xpm"));

	show();
}

