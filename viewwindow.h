

#ifndef __QPMD_VIEWWINDOW_H
#define __QPMD_VIEWWINDOW_H

#include <QWidget>

class PMDTreeView;
class QVBoxLayout;

class ViewWindow : public QWidget
{
	Q_OBJECT
public:
	ViewWindow(QWidget *parent = 0);

private:
	QVBoxLayout	*vbox;
	PMDTreeView	*tree;
};

#endif
