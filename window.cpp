
#include <QtGui>

#include <QHostInfo>
#include <QUdpSocket>

#include <Phonon/MediaObject>

#include <pmdp/pmdp.h>

#include <pmdapplication.h>
#include <pmdnetinterface.h>
#include <pmdcert.h>
#include <pmdpacket.h>
#include <pmdpacketrunner.h>
#include <window.h>
#include <viewwindow.h>

/*
 * Callback functions
 */
static int srvcb(const struct sockaddr *sa, int salen, void *ptr)
{
	(void)salen;
	Window *wp = (Window *)ptr;
	ListenAddress la(sa);
	if(-1 == wp->mcaddrport.indexOf(la))
		wp->mcaddrport.append(la);
	return 1;
}
/*
static int partcb(const pmdp_msg_t *msg, const pmdp_msg_part_t *part, void *ptr)
{
	print_part(part, (const char *)ptr);
	return 1;
}
*/
Window::Window()
{
	createActions();
	createTrayIcon();

	connect(trayIcon, SIGNAL(messageClicked()), this, SLOT(messageClicked()));
	connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

	QVBoxLayout *mainLayout = new QVBoxLayout;
	setLayout(mainLayout);

	setWindowIcon(QIcon(":/images/pingmydroid_144x144.xpm"));
	trayIcon->show();

	setWindowTitle(tr("PingMyDroid Client"));
	resize(400, 300);

	interfaces = PMDNetInterface::enumInterfaces();
	getDomnames();
#if 0
	for(int i = 0; i < interfaces.size(); i++) {
		PMDNetInterface *pip = interfaces[i];
		printf("Interface '%s' flags=%d\n", pip->getName().toUtf8().constData(), pip->getFlags());
		for(int j = 0; j < pip->ipv4addrs.size(); j++)
			printf("ipv4: '%s'\n", pip->ipv4addrs[j].toString().toUtf8().constData());
		for(int j = 0; j < pip->ipv6addrs.size(); j++)
			printf("ipv6: '%s'\n", pip->ipv6addrs[j].toString().toUtf8().constData());
	}
	for(int i = 0; i < domnames.size(); i++)
		printf("Name: '%s'\n", domnames[i].toUtf8().constData());
#endif
	mcaddrport.append(ListenAddress(PMDP_MCADDR, PMDP_PORT));
	mcaddrport.append(ListenAddress(PMDP_MCADDR6, PMDP_PORT));
	for(int i = 0; i < domnames.size(); i++) {
		pmdp_dns_srv_by_name(domnames[i].toUtf8().constData(), srvcb, this);
	}
	QList<quint16> ports;
	for(int i = 0; i < mcaddrport.size(); i++) {
		if(-1 == ports.indexOf(mcaddrport[i].port))
			ports.append(mcaddrport[i].port);
//		printf("mcaddrport: %s\n", mcaddrport[i].toString().toUtf8().constData());
	}

	for(int i = 0; i < ports.size(); i++) {
		for(int j = 0; j < mcaddrport.size(); j++) {
			if(ports[i] != mcaddrport[j].port)
				continue;
			bool isipv4 = mcaddrport[j].protocol() == QAbstractSocket::IPv4Protocol;
			bool r;
			QUdpSocket *sock = new QUdpSocket(this);
			if(isipv4)
				r = sock->bind(QHostAddress(QHostAddress::Any), mcaddrport[j].port, QUdpSocket::ShareAddress|QUdpSocket::ReuseAddressHint);
			else
				r = sock->bind(QHostAddress(QHostAddress::AnyIPv6), mcaddrport[j].port, QUdpSocket::ShareAddress|QUdpSocket::ReuseAddressHint);
			if(!r) {
				fprintf(stderr, "bind failed ipv%c\n", isipv4 ? '4' : '6');
				delete sock;
				continue;
			}
			sockets.append(sock);
			connect(sock, SIGNAL(readyRead()), this, SLOT(sockData()));
			for(int k = 0; k < interfaces.size(); k++) {
				if(isipv4 && interfaces[k]->ipv4addrs.size() > 0)
					sock->joinMulticastGroup(mcaddrport[j], interfaces[k]->interface());
				else if(!isipv4 && interfaces[k]->ipv6addrs.size() > 0)
					sock->joinMulticastGroup(mcaddrport[j], interfaces[k]->interface());
			}
		}
	}

	lastpacket = NULL;
	viewwindow = NULL;
}

void Window::sockData()
{
	for(int i = 0; i < sockets.size(); i++) {
		while(sockets[i]->hasPendingDatagrams()) {
			qint64 n = sockets[i]->pendingDatagramSize();
			if(-1 == n) {
				// FIXME: This should not happen, we have a datagram but no size...
				continue;
			}
			char *data = new char[n];
			QHostAddress ha;
			quint16 port;
			qint64 p = sockets[i]->readDatagram(data, n, &ha, &port);
			if(n != p) {
				// FIXME: WTF?
			}
			PMDPacketRunner *runner = new PMDPacketRunner(data, n, ha, port, this);
			connect(runner, SIGNAL(packetReady(PMDPacket *)), this, SLOT(handlePacket(PMDPacket *)), Qt::QueuedConnection);
			QThreadPool::globalInstance()->start(runner);
		}
	}
}

void Window::handlePacket(PMDPacket *pkt)
{
	if(!pkt->isValid()) {
		delete pkt;
		return;
	}
	if(!pmdApp->addPacket(pkt)) {
		delete pkt;
		return;
	}

	if(!pkt->forceSilent()) {
		if(!QSound::isAvailable()) {
			Phonon::MediaObject *music = Phonon::createPlayer(Phonon::MusicCategory, Phonon::MediaSource("ping.wav"));
			music->play();
		} else {
			QSound::play("ping.wav");
		}
	}
	//printf("Version: %u\n", pkt->getVersion());
	//printf("Severity: %d\n", pkt->getSeverity());
	//printf("Sequence: %u\n", pkt->getSequence());
	//printf("UUID: %s\n", pkt->getUuid().toString().toUtf8().constData());
	QString s = QString("%1\nCategory %2\nSubcategory %3\n")
			.arg(pkt->getTimestamp().toString("ddd MMMM d yyyy hh:mm:ss.zzz"))
			.arg(pkt->getCategory()->toString())
			.arg(pkt->getSubCategory()->toString());
	QSystemTrayIcon::MessageIcon micon;
	switch(pkt->getSeverity()) {
	case QPMDS_EMERGENCY:
	case QPMDS_ALERT:
	case QPMDS_CRITICAL:
	case QPMDS_ERROR:
		micon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Critical);
		break;
	case QPMDS_WARNING:
		micon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Warning);
		break;
	case QPMDS_NOTICE:
	case QPMDS_INFO:
	case QPMDS_DEBUG:
	default:
		micon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information);
		break;
	}
	trayIcon->showMessage(tr("%1PMD Message received").arg(pkt->haveEncrypted() ? "Encrypted " : ""), s, micon, 15000);
	lastpacket = pkt;
}

void Window::getDomname(const QHostAddress &addr)
{
	QHostInfo hi = QHostInfo::fromName(addr.toString());
	if(hi.error() == QHostInfo::NoError && hi.hostName() != addr.toString()) {
		QString hn = hi.hostName();
		int dot = hn.indexOf('.');
		if(-1 == dot)
			return;
		hn = hn.right(hn.size() - dot - 1);
		dot = hn.indexOf('.');
		if(-1 == dot || dot == hn.size())
			return;
		if(-1 == domnames.indexOf(hn))
			domnames.append(hn);
	}
}

void Window::getDomnames()
{
	for(int i = 0; i < interfaces.size(); i++) {
		for(int j = 0; j < interfaces[i]->ipv4addrs.size(); j++)
			getDomname(interfaces[i]->ipv4addrs[j]);
		for(int j = 0; j < interfaces[i]->ipv6addrs.size(); j++)
			getDomname(interfaces[i]->ipv6addrs[j]);
	}
}

void Window::setVisible(bool visible)
{
	minimizeAction->setEnabled(visible);
	maximizeAction->setEnabled(!isMaximized());
	restoreAction->setEnabled(isMaximized() || !visible);
	QDialog::setVisible(visible);
}

void Window::closeEvent(QCloseEvent *event)
{
	if(trayIcon->isVisible()) {
		hide();
		event->ignore();
	}
}

void Window::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
	switch(reason) {
	case QSystemTrayIcon::Trigger:
	case QSystemTrayIcon::DoubleClick:
		showViewWindow();
		break;
	case QSystemTrayIcon::MiddleClick:
		break;
	default:
	;
	}
}

void Window::destroyView()
{
	viewwindow = NULL;
}

void Window::messageClicked()
{
	if(!lastpacket) {
		lastpacket = pmdApp->getPacket(-1);
		if(!lastpacket)
			return;
	}
	showViewWindow();
}

void Window::showViewWindow()
{
	// Open window to show message(s)
	if(viewwindow) {
		viewwindow->show();
	} else {
		viewwindow = new ViewWindow();
		connect(viewwindow, SIGNAL(destroyed()), this, SLOT(destroyView()));
	}
}

void Window::loadCertificate()
{
	QStringList sl;
	QFileDialog fd(this, tr("Load Certificate(s)"));
	fd.setNameFilter(tr("Certificate files (*.crt *.pem)"));
	fd.setFileMode(QFileDialog::ExistingFiles);
	fd.setHistory(fdhistory);
	fd.setDirectory(fddirectory);
	if(fd.exec()) {
		fdhistory = fd.history();
		fddirectory = fd.directory().absolutePath();
		sl = fd.selectedFiles();
		for(int i = 0; i < sl.size(); i++) {
			pmdApp->addCrt(new PMDCert(sl[i], sl[i]));
		}
	}
}

void Window::createActions()
{
	minimizeAction = new QAction(tr("Mi&nimize"), this);
	connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

	maximizeAction = new QAction(tr("Ma&ximize"), this);
	connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

	certificateAction = new QAction(tr("&Load Certificate"), this);
	connect(certificateAction, SIGNAL(triggered()), this, SLOT(loadCertificate()));

	restoreAction = new QAction(tr("&Open"), this);
	connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

	quitAction = new QAction(tr("&Quit"), this);
	connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void Window::createTrayIcon()
{
	trayIconMenu = new QMenu(this);
	trayIconMenu->addAction(minimizeAction);
	trayIconMenu->addAction(maximizeAction);
	trayIconMenu->addAction(certificateAction);
	trayIconMenu->addAction(restoreAction);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(quitAction);

	trayIcon = new QSystemTrayIcon(this);
	trayIcon->setContextMenu(trayIconMenu);
	trayIcon->setIcon(QIcon(":/images/pingmydroid_144x144.xpm"));
}
