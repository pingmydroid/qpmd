
#ifndef __QPMD_WINDOW_H
#define __QPMD_WINDOW_H

#include <QSystemTrayIcon>
#include <QDialog>
#include <QHostAddress>
#include <QFutureWatcher>

#include <netinet/in.h>

class QAction;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QPushButton;
class QSpinBox;
class QTextEdit;

class PMDNetInterface;
class PMDPacket;
class QUdpSocket;

class ViewWindow;

class ListenAddress : public QHostAddress
{
public:
	ListenAddress(const QString &hname, quint16 p) : QHostAddress(hname) {
		port = p;
	};
	ListenAddress(const sockaddr *sa) : QHostAddress(sa) {
		switch(protocol()) {
		case QAbstractSocket::IPv4Protocol:
			port = ntohs(((const struct sockaddr_in *)sa)->sin_port);
			break;
		case QAbstractSocket::IPv6Protocol:
			port = ntohs(((const struct sockaddr_in6 *)sa)->sin6_port);
			break;
		default:
			port = 0;
			break;
		}
	};

	ListenAddress(const ListenAddress &la) : QHostAddress(la) {
		port = la.port;
	}

	QString toString() const { return protocol() == QAbstractSocket::IPv4Protocol ? QString("%1:%2").arg(QHostAddress::toString()).arg(port) : QString("[%1]:%2").arg(QHostAddress::toString()).arg(port); };

	quint16	port;

	bool operator==(const ListenAddress &la) { return la.toString() == toString() && la.port == port; }
	bool operator!=(const ListenAddress &la) { return la.toString() != toString() || la.port != port; }
};

class Window : public QDialog
{
    Q_OBJECT

public:
    Window();

    void setVisible(bool visible);

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void messageClicked();

private:
	void createActions();
	void createTrayIcon();

	QAction		*minimizeAction;
	QAction		*maximizeAction;
	QAction		*certificateAction;
	QAction		*restoreAction;
	QAction		*quitAction;

	ViewWindow	*viewwindow;

	QSystemTrayIcon	*trayIcon;
	QMenu		*trayIconMenu;

	PMDPacket	*lastpacket;

	void getDomname(const QHostAddress &addr);
	void getDomnames();
	void showViewWindow();

	QStringList	fdhistory;
	QString		fddirectory;

private slots:
	void sockData();
	void handlePacket(PMDPacket *);
	void destroyView();
	void loadCertificate();

signals:

public:
	QList<PMDNetInterface *> interfaces;
	QList<QString>		domnames;
	QList<ListenAddress>	mcaddrport;
	QList<QUdpSocket *>	sockets;

};

#endif
